import React, { useState } from 'react'
import { Alert, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'

export const TodoInput = ({ updateTodoList }) => {

  const [todoInput, setTodoInput] = useState('')

  const submitTodoHandler = () => {
    if (todoInput === '') return Alert.alert('Input must not empty!')
    updateTodoList(todoInput)
    setTodoInput('')
  }

  return (
    <View style={{ flexDirection: 'row', paddingVertical: 20, paddingHorizontal: 20 }}>
      <View style={{ flex: 5 }}>
        <TextInput onChangeText={(val) => setTodoInput(val)} value={todoInput} multiline style={{ borderBottomColor: '#0be6bb', borderBottomWidth: 1 }} />
      </View>
      <TouchableOpacity onPress={submitTodoHandler} style={styles.button}>
        <Text style={styles.buttonText}>Add Todo</Text>
      </TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
  button: {
    paddingHorizontal: 10,
    paddingVertical: 4,
    borderRadius: 8,
    backgroundColor: '#0be6bb',
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 20
  },
  buttonText: {
    color: '#eee'
  }
})