/**
 * @format
 */
import React, { useState } from 'react'
import { AppRegistry, Text, View, StyleSheet } from 'react-native'
import { name as appName } from './app.json'
import { TodoInput } from './components/TodoInput'

const App = () => {

  const [todoList, setTodoList] = useState([])

  const updateTodoList = (val) => {
    const newTodoList = [...todoList]
    newTodoList.push(val)
    setTodoList(newTodoList)
  }

  const _renderTodoList = () => {
    return todoList.map(todo => <Text>{todo}</Text>)
  }

  return (
    <View style={styles.container}>
      <View>
        <TodoInput updateTodoList={updateTodoList} />
      </View>
      <View>
        {_renderTodoList()}
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
})

AppRegistry.registerComponent(appName, () => App)
